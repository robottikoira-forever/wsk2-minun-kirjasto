# Esittely

Oma kirjasto on sovellus omien kirjojen katalogisointia ja lainausta varten. Käyttäjän on mahdollista lisätä kirjoja sovellukseen hakemalla Finnan avoimesta rajapinnasta sekä manuaalisesti tietoja lisäämällä.  

Oma kirjasto REST API:lla varustettu React-sovellus, jonka back end on toteutettu Node.js:llä ja MySQL-tietokannalla. Tietokanta koostuu kolmesta taulusta, jotka sisältävät tiedot kirjoista, lainaajista ja lainat sisältävästä liitostaulusta, joka yhdistää lainatut kirjat lainaajiin. 


## GET-pyynnöt 

### Kirjat 

GET-pyyntö osoitteeseen ```http://localhost:8081/kirjat``` palauttaa kaikken tietokantaan listattujen kirjojen tiedot. JSON-vastauksena saadaan lista, jonka koostavien tietueiden kentät ovat:
- ```id``` (kokonaisluku)
- ```nimi``` (kirjan nimi merkkijonona)
- ```kirjailija``` (merkkijono)
- ```kieli``` (merkkijono)
- ```kuva``` (kirjan kansikuvan paikallinen sijainti palvelimella)
- ```vapaa``` (totuusarvo, joka kertoo liittyykö ko. kirjaan aktiivista lainaa)

### Lainaajat 

GET-pyyntö osoitteeseen ```http://localhost:8081/lainaajat``` palauttaa kaikken tietokantaan listattujen lainaajien tiedot. JSON-vastauksena saadaan lista, jonka koostavien tietueiden kentät ovat:
- ```id``` (kokonaisluku)
- ```nimi``` (merkkijono)

### Lainat 

GET-pyyntö osoitteeseen ```http://localhost:8081/lainat``` palauttaa kaikken tietokantaan listattujen lainojen tiedot. JSON-vastauksena saadaan lista, jonka koostavien tietueiden kentät ovat:
- ```kirja_nimi``` (merkkijono)
- ```lainaaja_nimi``` (merkkijono)
- ```id``` (kokonaisluku)
- ```lainauspaiva``` (merkkijono)
- ```palautuspaiva``` (merkkijono)
- ```kirja_id``` (kokonaisluku)
- ```lainaaja_id``` (kokonaisluku)


## POST-pyynnöt 

### Kirjat 

Tietokantaan voi lisätä kirjan tiedot POST-pyynnöllä osoitteeseen ```http://localhost:8081/kirjat```. Kirjan tiedot välitetään pyynnön body-osassa JSON:ksi kirjoitettuna. Tämän mahdollistamiseksi on myös määritettävä headers-osaan kenttä ```“Content-Type”``` ja sen arvoksi ```“application/json”```. Välitettävälle JSON-oliolle kuuluu antaa kentät:
- ```nimi``` (merkkijono)
- ```kirjailija``` (merkkijono)
- ```kieli``` (merkkijono)
- ```kuva``` (merkkijono tai null)

Jos kuvakenttään on annettu linkki, kirjan POST-pyyntö käynnistää samalla myös kuvan latauksen.  Palvelin kopioi kyseisestä osoitteesta kuvan ja tallentaa tietokantaan paikallisen kuvakopion nimen. 

### Lainaajat 

Tietokantaan voi lisätä lainaajan tiedot POST-pyynnöllä osoitteeseen ```http://localhost:8081/lainaajat```. Lainaajan tiedot välitetään pyynnön body-osassa JSON:ksi kirjoitettuna. Tämän mahdollistamiseksi on myös määritettävä headers-osaan kenttä ```“Content-Type”``` ja sen arvoksi ```“application/json”```. Välitettävälle JSON-oliolle kuuluu antaa kenttä ```nimi``` (merkkijono). 

### Lainat 

Tietokantaan voi lisätä lainan tiedot POST-pyynnöllä osoitteeseen ```http://localhost:8081/lainat```. Lainan tiedot välitetään pyynnön body-osassa JSON:ksi kirjoitettuna. Tämän mahdollistamiseksi on myös määritettävä headers-osaan kenttä ```“Content-Type”``` ja sen arvoksi ```“application/json”```. Välitettävälle JSON-oliolle kuuluu antaa kentät:
- ```kirja_id``` (kokonaisluku)
- ```lainaaja_id``` (kokonaisluku)
- ```lainauspäivä``` (merkkijono muodossa VVVV-KK-PP)

Huom! ```lainaaja_id```:n ja ```kirja_id```:n on vastattava tietokannasta löytyviä vastaavia arvoja. 


## DELETE-pyynnöt 

### Kirjat 

DELETE-pyyntö osoitteeseen ```http://localhost:8081/kirjat/id```, jossa ```id```:n paikalle syötetään poistettavan tietueen ```id```, poistaa kyseisen tietueen tietokannasta. 

### Lainaajat 

DELETE-pyyntö osoitteeseen ```http://localhost:8081/lainaajat/id```, jossa ```id```:n paikalle syötetään poistettavan tietueen ```id```, poistaa kyseisen tietueen tietokannasta. 


## PUT-pyynnöt 

### Lainat 

Tietokantaan voi merkitä kirjan palautetuksi PUT-pyynnöllä osoitteeseen ```http://localhost:8081/lainat/id```, jossa ```id``` korvataan muutettavan tietueen ```id```:llä. Kutsuun syötetty ```id``` kohdistaa muokkauskutsun kyseiseen tietueeseen tietokannassa. PUT-kutsun headers-osaan ```“Content-Type”```:n arvoksi annetaan ```“application/json”```. Kutsun body-osaan tulee liittää tietosisältönä palautuspäivä JSON-oliona muodossa ```“palautuspäivä”: “VVVV-KK-PP”```.  

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
