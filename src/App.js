import './App.css';
import Navigointi from "./components/Navigointi";

function App() {
    return (
        <div className="App">
            <Navigointi/>
        </div>
    );
}

export default App;
