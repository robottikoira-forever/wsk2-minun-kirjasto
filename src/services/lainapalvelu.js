import axios from 'axios'

const url = 'http://localhost:8081/lainat'

const lisaa = uusiLaina =>
    axios
        .post(url, uusiLaina)
        .then(vastaus => vastaus.data)

const haeKaikki = () =>
    axios
        .get(url)
        .then(vastaus => vastaus.data)

const palauta = (palautuspäivä, id) =>
    axios
        .put(`${url}/${id}`, {palautuspäivä})
        .then(vastaus => vastaus.data)

export default {lisaa, haeKaikki, palauta}