import React from "react";
import axios from "axios";

const url = 'https://api.finna.fi/v1/search?filter[]=format:"0/Book/"&lookfor='

const siivoa = (hakusana) => hakusana.toLowerCase().split(' ').join('+')

const hae = (hakusana) =>
    axios
        .get(`${url}${siivoa(hakusana)}`)
        .then(vastaus => vastaus.data)

export default {hae}