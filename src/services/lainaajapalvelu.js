import axios from 'axios'

const url = 'http://localhost:8081/lainaajat'

const lisaa = uusiLainaaja =>
    axios
        .post(url, uusiLainaaja)
        .then(vastaus => vastaus.data)

const haeKaikki = () =>
    axios
        .get(url)
        .then(vastaus => vastaus.data)

const poistaLainaaja = (id) =>
    axios
        .delete(`${url}/${id}`)

export default {lisaa, haeKaikki, poistaLainaaja}