import axios from 'axios'

const url = 'http://localhost:8081/kirjat'

const lisaa = uusiKirja =>
    axios
        .post(url, uusiKirja)
        .then(vastaus => vastaus.data)

const haeKaikki = () =>
    axios
        .get(url)
        .then(vastaus => vastaus.data)

const poistaKirja = (id) =>
    axios
        .delete(`${url}/${id}`)

export default {lisaa, haeKaikki, poistaKirja}