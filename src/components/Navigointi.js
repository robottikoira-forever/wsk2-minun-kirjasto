import {useState} from "react";
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom"
import {Table, Nav, Navbar} from 'react-bootstrap'
import Kirjahylly from "./kirjat/Kirjahylly";
import Lainaajasivu from "./lainaajat/Lainaajasivu";
import Lainasivu from "./lainat/Lainasivu";


const Navigointi = () => {
    const tilat = {
        SUODATUS: 'suodatus',
        FINNA: 'finna',
        LISÄYS: 'lisäys'
    }
    const [tila, setTila] = useState(tilat.SUODATUS)

    return (
        <>
            <div className='tausta'></div>

            <Router>
                <Navbar className='navigaatio-palkki' variant='dark'>
                    <div className='sisältö'>
                        <span className='navigaatio-otsikko'>Oma kirjasto</span>
                        <Link to="/" className='navigaatio-linkki'>Kirjahylly</Link>
                        <Link to="/lainat" className='navigaatio-linkki'>Lainat</Link>
                        <Link to="/lainaajat" className='navigaatio-linkki'>Lainaajat</Link>
                    </div>
                </Navbar>

                <Switch>
                    <Route path="/lainat">
                        <div className='sisältö'>
                            <Lainasivu/>
                        </div>
                    </Route>
                    <Route path="/lainaajat">
                        <div className='sisältö'>
                            <Lainaajasivu/>
                        </div>
                    </Route>
                    <Route path="/">
                        <div className='sisältö'>
                            <Kirjahylly
                                tilat={tilat}
                                tila={tila}
                                setTila={setTila}
                            />
                        </div>
                    </Route>
                </Switch>
            </Router>
        </>
    )
}

export default Navigointi