import React, {useState, useEffect} from "react";
import {Modal, Button, Form} from "react-bootstrap";
import kirjapalvelu from "../services/kirjapalvelu";
import lainaajapalvelu from "../services/lainaajapalvelu";
import lainapalvelu from "../services/lainapalvelu";

const Lainausikkuna = ({näytä, sulje, nimi, konteksti, id}) => {
    const [data, setData] = useState([])
    const [virhe, setVirhe] = useState(false)

    const valinnat = data.map(datum => {
        return (
            <option value={datum.id} key={datum.id}>{datum.nimi}</option>
        )
    })

    const virheilmoitus = ((virhe) => {
        if (virhe) {
            return <div>Lainauksen tallennus epäonnistui!</div>
        }
    })(virhe)

    const tekstit = {
        'kirjat': {
            'otsikko1': 'Lainaa',
            'otsikko2': 'henkilölle',
            'id': 'lainaaja',
            'piilo_id': 'kirja'
        },
        'lainaajat': {
            'otsikko1': 'Lainaa henkilölle',
            'otsikko2': '',
            'id': 'kirja',
            'piilo_id': 'lainaaja'
        }
    }

    const päivämäärä = () => {
        const pvm = new Date()
        return `${pvm.getFullYear()}-${pvm.getMonth()+1}-${pvm.getDate()}`

    }

    const arvot = tekstit[konteksti]

    const lainanLisäys = () => {
        const laina = {
            lainaaja_id: document.getElementById('lainaaja').value,
            kirja_id: document.getElementById('kirja').value,
            lainauspäivä: päivämäärä()
        }
        lainapalvelu
            .lisaa(laina).then(data => {
            if (!data) {
                setVirhe(true)
            } else {
                sulje()
            }
        })
    }

    useEffect(() => {
        switch (konteksti) {
            case 'lainaajat':
                kirjapalvelu
                    .haeKaikki()
                    .then(kirjat => setData(kirjat.filter(kirja => kirja.vapaa)))
                break
            case 'kirjat':
                lainaajapalvelu
                    .haeKaikki()
                    .then(lainaajat => setData(lainaajat))
                break
            default:
                break
        }
    }, [])

    return (
        <Modal
            show={näytä}
            centered
            size='lg'
            backdrop='static'
        >
            <Modal.Header>
                <Modal.Title>{arvot.otsikko1} <em style={{fontSize: 28, fontWeight: 400}}>{nimi}</em> {arvot.otsikko2}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <input type='hidden' id={arvot.piilo_id} value={id}/>
                <select id={arvot.id} class='form-control'>
                    <option disabled selected>Valitse {arvot.id}</option>
                    {valinnat}
                </select>
                {virheilmoitus}
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={sulje} variant="light">Sulje</Button>
                <Button onClick={lainanLisäys} className='nappi-petrooli'>Lainaa</Button>
            </Modal.Footer>
        </Modal>
    )
}
export default Lainausikkuna