import React, {useState} from 'react'
import {Button, Col, Form} from "react-bootstrap";
import lainaajapalvelu from "../../services/lainaajapalvelu";

const LisaaLainaaja = ({setPäivitä}) => {
    const [nimi, setNimi] = useState('')

    const muuta = (event) => {
        setNimi(event.target.value)
    }

    const tyhjennä = () => {
        setNimi('')
    }

    const lisaa = (event) => {
        event.preventDefault()

        const lainaaja = {
            nimi
        }
        lainaajapalvelu
            .lisaa(lainaaja)
            .then(() => {
                tyhjennä()
                setPäivitä(true)
            })

    }

    return (
        <div className='pohja topattu-vähän topattu-vähän-lisää-alhaalta'>
        <Form onSubmit={lisaa}>
            <Form.Row className='justify-content-md-center'>
                <Col xs='4'>
                    <Form.Control
                        autocomplete='off'
                        id='nimiKenttä'
                        type='text'
                        value={nimi}
                        onChange={muuta}
                        placeholder='Anna lainaajan nimi'
                    />
                </Col>
                <Button className='nappi' type='submit'>Lisää</Button>
            </Form.Row>
        </Form>
        </div>
    )
}

export default LisaaLainaaja
