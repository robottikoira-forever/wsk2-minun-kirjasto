import React, {useEffect, useState} from 'react'
import {Button, ListGroup} from 'react-bootstrap'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrashAlt} from "@fortawesome/free-regular-svg-icons";
import Lainausikkuna from "../Lainausikkuna";
import lainaajapalvelu from "../../services/lainaajapalvelu";
import Vahvistusikkuna from "../Vahvistusikkuna";

const Lainaaja = ({lainaaja, setPäivitä}) => {
    const [avaaLainaus, setAvaaLainaus] = useState(false)
    const [avaaPoisto, setAvaaPoisto] = useState(false)
    const [poista, setPoista] = useState(false)
    const {id, nimi} = lainaaja

    const sisältö = `Haluatko varmasti poistaa lainaajan ${nimi} ja häneen liittyvät lainatapahtumat?`

    useEffect(() => {
        if (poista)
            lainaajapalvelu
                .poistaLainaaja(id)
                .then(() => {
                    setPäivitä(true);
                    setPoista(false)
                })
    }, [poista])

    return (
        <>
            <ListGroup.Item className='pohja vaaleat-reunat'>
                <div className='oikealle'>
                    <Button className='nappi' size='lg' onClick={() => setAvaaLainaus(true)}>Lainaa</Button>
                    <FontAwesomeIcon
                        className='roskis'
                        onClick={() => setAvaaPoisto(true)}
                        icon={faTrashAlt}/>
                </div>
                <div className='kirja-tiedot'>
                    <strong>{nimi}</strong>
                </div>
            </ListGroup.Item>
            <Lainausikkuna
                konteksti='lainaajat'
                id={id}
                nimi={nimi}
                näytä={avaaLainaus}
                sulje={() => setAvaaLainaus(false)}
            />
            <Vahvistusikkuna
                näytä={avaaPoisto}
                sulje={() => setAvaaPoisto(false)}
                otsikko='Vahvista lainaajan poisto'
                sisältö={sisältö}
                vahvista={() => setPoista(true)}
            />
        </>
    )
}

export default Lainaaja