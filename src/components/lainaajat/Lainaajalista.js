import React, {useEffect, useState} from 'react'
import {ListGroup} from 'react-bootstrap'
import lainaajapalvelu from "../../services/lainaajapalvelu";
import Lainaaja from "./Lainaaja";

const Lainaajalista = ({päivitä, setPäivitä}) => {
    const [lainaajat, setLainaajat] = useState([])

    const lista = lainaajat.map(lainaaja => {
        return (
            <Lainaaja
                key={lainaaja.id}
                lainaaja={lainaaja}
                setPäivitä={setPäivitä}
            />
        )
    })

    useEffect(() => {
        if (päivitä)
            lainaajapalvelu
                .haeKaikki()
                .then(haetutLainaajat => {
                    setLainaajat(haetutLainaajat)
                    setPäivitä(false)
                })
    }, [päivitä])

    return (
        <ListGroup>
            {lista}
        </ListGroup>
    )
}

export default Lainaajalista