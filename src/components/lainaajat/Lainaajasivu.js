import React, {useState} from 'react'
import LisaaLainaaja from "./LisaaLainaaja";
import Lainaajalista from "./Lainaajalista";

const Lainaajasivu = () => {
    const [päivitä, setPäivitä] = useState(true)

    return (
        <div>
            <LisaaLainaaja setPäivitä={setPäivitä}/>
            <Lainaajalista päivitä={päivitä} setPäivitä={setPäivitä}/>
        </div>

    )
}

export default Lainaajasivu