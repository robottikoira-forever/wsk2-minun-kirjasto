import {useEffect, useState} from 'react'
import {Button, ListGroup} from 'react-bootstrap'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrashAlt} from "@fortawesome/free-regular-svg-icons";
import {faFileImage, faImage} from "@fortawesome/free-solid-svg-icons";
import Lainausikkuna from "../Lainausikkuna";
import Vahvistusikkuna from "../Vahvistusikkuna";
import kirjapalvelu from "../../services/kirjapalvelu";

const Kirja = ({kirja, setPäivitä}) => {
    const [avaaLainaus, setAvaaLainaus] = useState(false)
    const [avaaPoisto, setAvaaPoisto] = useState(false)
    const [poista, setPoista] = useState(false)
    const {id, nimi, kirjailija, kieli, vapaa} = kirja
    const kuvaosoite = kirja.kuva
    const oletuskuvaosoite = '/resources/missing2.png'
    const kuva = kuvaosoite === null
        ? <img src={oletuskuvaosoite} className='kuvake-pieni'/>
        : <img src={kuvaosoite} className='kuvake'/>

    const lainattavissa = vapaa
        ? <Button
            className='nappi'
            size='lg'
            onClick={() => setAvaaLainaus(true)}
        >Lainaa</Button>
        : <div className='lainassa'>
            <span>Lainassa</span>
        </div>

    const sisältö = `Haluatko varmasti poistaa kirjan ${nimi} ja siihen liittyvät lainatapahtumat?`

    useEffect(() => {
        if (poista)
            kirjapalvelu
                .poistaKirja(id)
                .then(() => {
                    setPäivitä(true);
                    setPoista(false)
                })
    }, [poista])


    return (
        <>
            <ListGroup.Item className='pohja vaaleat-reunat'>
                <div className='oikealle toiminnot'>
                    {lainattavissa}
                    <FontAwesomeIcon
                        className='roskis'
                        onClick={() => setAvaaPoisto(true)}
                        icon={faTrashAlt}
                    /></div>
                <div className='kuvake-säiliö vasemmalle'>{kuva}</div>
                <div className='kirja-tiedot'>
                    <div><strong>{nimi}</strong></div>
                    <div>{kirjailija}</div>
                    <div>{kieli}</div>
                </div>
            </ListGroup.Item>
            <Lainausikkuna
                konteksti='kirjat'
                id={id}
                nimi={nimi}
                näytä={avaaLainaus}
                sulje={() => {
                    setAvaaLainaus(false)
                    setPäivitä(true)
                }}
            />
            <Vahvistusikkuna
                näytä={avaaPoisto}
                sulje={() => setAvaaPoisto(false)}
                otsikko='Vahvista kirjan poisto'
                sisältö={sisältö}
                vahvista={() => setPoista(true)}
            />
        </>
    )
}

export default Kirja