import React from 'react';
import {Col, Form} from "react-bootstrap";

const Suodatus = ({rajaus, setRajaus}) => {

    const haeNimellä = () => {
        const haku = document.getElementById('haku').value
        setRajaus({
            ...rajaus,
            haku
        })
    }

    const suodataKirjat = () => {
        const näytä = document.getElementById('näytä').value
        setRajaus({
            ...rajaus,
            näytä
        })
    }

    return (
        <div className='pohja topattu-vähän topattu-vähän-lisää-alhaalta'>
            <Form>
                <Form.Row className='justify-content-md-center'>
                        <Col  xs='3'>
                            <Form.Control
                                id='haku'
                                type='text'
                                placeholder='Hae...'
                                onChange={haeNimellä}
                                value={rajaus.haku}
                                autocomplete='off'
                            />
                        </Col>
                        <Col xs='3'>
                            <Form.Control xs='6' as='select' id='näytä' onChange={suodataKirjat} className='pudotusvalikko'>
                                <option value='kaikki'>Kaikki</option>
                                <option value='vapaat'>Vapaana</option>
                                <option value='lainatut'>Lainassa</option>
                            </Form.Control>
                        </Col>
                </Form.Row>
            </Form>
        </div>
    )
}

export default Suodatus