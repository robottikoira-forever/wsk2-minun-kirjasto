import React, {useState} from 'react'
import Kirjalista from './Kirjalista'
import LisaaKirja from './LisaaKirja'
import Suodatus from "./Suodatus";
import FinnaHaku from "./FinnaHaku";
import FinnaLista from "./FinnaLista";

const Kirjahylly = ({tilat, tila, setTila}) => {
    const [päivitä, setPäivitä] = useState(true)
    const [hakusana, setHakusana] = useState('')
    const [rajaus, setRajaus] = useState({
        haku: '',
        näytä: 'kaikki'
    })
    const {SUODATUS, FINNA, LISÄYS} = tilat

    const lisäys = <LisaaKirja
        setPäivitä={setPäivitä}
    />

    const suodatus = <Suodatus
        rajaus={rajaus}
        setRajaus={setRajaus}
    />

    const finnaHaku = <FinnaHaku
        setHakusana={setHakusana}
    />

    const kirjalista = <Kirjalista
        päivitä={päivitä}
        setPäivitä={setPäivitä}
        rajaus={rajaus}
    />

    const finnaLista = <FinnaLista
        hakusana={hakusana}
    />

    const täyte = <div className='pohja täyte'></div>

    const tilanVaihto = (tila) => {
        setTila(tila)
        setPäivitä(true)
    }

    return (
        <>
            <div className='pohja topattu-vähän'>
                <span
                    onClick={() => tilanVaihto(SUODATUS)}
                    className={`klikattava${tila === SUODATUS ? ' klikattava-valittu' : ''}`}
                >Omat kirjat</span>
                {' | '}
                <span
                    onClick={() => tilanVaihto(FINNA)}
                    className={`klikattava${tila === FINNA ? ' klikattava-valittu' : ''}`}
                >Hae uusia kirjoja</span>
                {' | '}
                <span
                    onClick={() => tilanVaihto(LISÄYS)}
                    className={`klikattava${tila === LISÄYS ? ' klikattava-valittu' : ''}`}
                >Lisää kirja lomakkeella</span>
            </div>
            {
                tila === SUODATUS
                    ? suodatus
                    : tila === LISÄYS
                        ? lisäys
                        : tila === FINNA
                            ? finnaHaku
                            : täyte
            }
            {
                tila === SUODATUS
                    ? kirjalista
                    : tila === FINNA
                    ? finnaLista : täyte}
        </>

    )
}

export default Kirjahylly