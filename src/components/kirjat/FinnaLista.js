import React, {useState, useEffect} from "react";
import {ListGroup} from "react-bootstrap";
import finnapalvelu from "../../services/finnapalvelu";
import FinnaKirja from "./FinnaKirja";

const FinnaLista = ({hakusana}) => {
    const [kirjat, setKirjat] = useState([])
    const [eiOsumia, setEiOsumia] = useState(false)

    const lista = kirjat.map(kirja => {
        return (
            <FinnaKirja
                key={kirja.id}
                kirja={kirja}
            />
        )
    })

    useEffect(() => {
        if (hakusana.length > 0) {
            finnapalvelu
                .hae(hakusana)
                .then(data => {

                    if (data != null && data.records != null && Array.isArray(data.records)) {
                        setKirjat([])

                        const priorisoidut = {
                            kuvalliset: [],
                            kuvattomat: []
                        }

                        data.records.forEach(kirja => {
                            const tärkeätKielet = ['fin', 'eng', 'swe']
                            const kielet = kirja.languages
                            for (let i = 0; i < kielet.length; i++) {
                                if (!tärkeätKielet.includes(kielet[i])) {
                                    return
                                }
                            }
                            if (kirja.images.length > 0)
                                priorisoidut.kuvalliset.push(kirja)
                            else
                                priorisoidut.kuvattomat.push(kirja)
                        })
                        const priorisoimattomat = data.records.filter(kirja =>
                            !priorisoidut.kuvalliset.includes(kirja)
                            && !priorisoidut.kuvattomat.includes(kirja))
                        setKirjat([...priorisoidut.kuvalliset, ...priorisoidut.kuvattomat, ...priorisoimattomat])
                        setEiOsumia(false)
                    }
                    else{
                        setEiOsumia(true)
                    }
                })

        }
    }, [hakusana])

    if(eiOsumia){
        return (
            <div className='täyte pohja'>
            <ListGroup>
                <ListGroup.Item className='pohja'>
                    Ei osumia!
                </ListGroup.Item>
            </ListGroup>
        </div>)
    }

    return (
        <div className='täyte pohja'>
            <ListGroup>
                {lista}
            </ListGroup>
        </div>
    )
}

export default FinnaLista