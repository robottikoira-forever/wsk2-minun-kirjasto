import React, {useState} from "react";
import {Button, ListGroup} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFileImage} from "@fortawesome/free-solid-svg-icons";
import kirjapalvelu from "../../services/kirjapalvelu";

const FinnaKirja = ({kirja}) => {
    const [lisätty, setLisätty] = useState(false)
    const nimet = kirja.title.split(':')
    const pääotsikko = nimet[0].trim()
    const aliotsikko = nimet.length > 1 ? nimet[1].trim() : null
    const kuvaosoite = kirja.images.length > 0 ? `https://www.finna.fi${kirja.images[0]}` : null
    const oletuskuvaosoite = '/resources/missing2.png'
    const kuva = kuvaosoite !== null
        ? <img src={kuvaosoite} className='kuvake'/>
        : <img src={oletuskuvaosoite} className='kuvake-pieni'/>
    const kielet = kirja.languages.join(', ')

    const muotoileNimi = (nimi) => {
        let kirjailija = nimi.split(', ')
        if (kirjailija.length > 1)
            return `${kirjailija[1]} ${kirjailija[0]}`
        else
            return kirjailija[0]
    }

    const kirjailijat = kirja.nonPresenterAuthors.map(kirjailija => muotoileNimi(kirjailija.name)).join(', ')

    const lisää = () => {
        const uusiKirja = {
            nimi: aliotsikko !== null ? `${pääotsikko}: ${aliotsikko}` : pääotsikko,
            kuva: kuvaosoite,
            kieli: kielet,
            kirjailija: kirjailijat
        }
        kirjapalvelu
            .lisaa(uusiKirja)
            .then(() => setLisätty(true))
    }

    return (
        <ListGroup.Item className='pohja vaaleat-reunat'>
            <div className='oikealle toiminnot'>
                {lisätty ? <span>Kirja on lisätty hyllyyn</span> :
                    <Button className='nappi' size='lg' onClick={lisää}>Lisää kirjahyllyyn</Button>}
            </div>

            <div className='vasemmalle kuvake-säiliö'>{kuva}</div>

            <div className='kirja-tiedot'>
                <div>
                    <strong>{pääotsikko}</strong>
                </div>

                <div>
                    <strong>{aliotsikko}</strong>
                </div>

                <div>
                    {kirjailijat}
                </div>

                <div>
                    {kielet}
                </div>
            </div>
        </ListGroup.Item>
    )
}

export default FinnaKirja