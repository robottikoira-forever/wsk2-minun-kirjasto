import React from "react";
import {Button, Col, Form} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch} from "@fortawesome/free-solid-svg-icons";

const FinnaHaku = ({setHakusana}) => {
    const muutaHakusanaa = (event) => {
        event.preventDefault()
        console.log('muutaHakusanaa kutsuttu')
        const uusiHakusana = document.getElementById('finnaKenttä').value
        setHakusana(uusiHakusana)
    }

    return (
        <div className='pohja topattu-vähän topattu-vähän-lisää-alhaalta'>
            <Form onSubmit={muutaHakusanaa}>
                <Form.Row className='justify-content-md-center'>
                    <Col xs='4'>
                        <Form.Control
                            type='text'
                            id='finnaKenttä'
                            placeholder='Hae...'
                            autocomplete='off'
                        />
                    </Col>
                    <Button className='nappi' type='submit'>
                        <FontAwesomeIcon icon={faSearch}/>
                    </Button>
                </Form.Row>
            </Form>
        </div>
    )
}

export default FinnaHaku