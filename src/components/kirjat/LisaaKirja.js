import React, {useState} from 'react'
import {Col, Button, Form, Modal} from "react-bootstrap";
import kirjapalvelu from '../../services/kirjapalvelu'

const LisaaKirja = ({setPäivitä}) => {
    const [nimi, setNimi] = useState('')
    const [kirjailija, setKirjailija] = useState('')
    const [kuvaosoite, setKuvaosoite] = useState('')
    const [kieli, setKieli] = useState('')
    const [viesti, setViesti] = useState('')
    const [onnistui, setOnnistui] = useState(false)
    const [virheet, setVirheet] = useState({
        nimi: false,
        kirjailija: false,
        kieli: false
    })

    const muuta = (event) => {
        const id = event.target.id
        const value = event.target.value

        switch (id) {
            case 'nimiKenttä':
                if (value.length > 0)
                    setVirheet({...virheet, nimi: false})
                setNimi(value)
                break
            case 'kirjailijaKenttä':
                if (value.length > 0)
                    setVirheet({...virheet, kirjailija: false})
                setKirjailija(value)
                break
            case 'kieliKenttä':
                if (value.length > 0)
                    setVirheet({...virheet, kieli: false})
                setKieli(value)
                break
            case 'kuvaosoiteKenttä':
                setKuvaosoite(value)
                break
            default:
                break
        }
    }

    const tyhjennä = () => {
        setNimi('')
        setKirjailija('')
        setKuvaosoite('')
        setKieli('')
    }

    const lisaa = (event) => {
        event.preventDefault()

        const uudetVirheet = {
            nimi: false,
            kirjailija: false,
            kieli: false
        }

        if (nimi.length === 0)
            uudetVirheet.nimi = true
        if (kirjailija.length === 0)
            uudetVirheet.kirjailija = true
        if (kieli.length === 0)
            uudetVirheet.kieli = true

        setVirheet(uudetVirheet)

        if (uudetVirheet.nimi || uudetVirheet.kirjailija || uudetVirheet.kieli)
            return

        const kirja = {
            nimi,
            kirjailija,
            kieli: null,
            kuva: null
        }
        if (kieli.length > 0)
            kirja.kieli = kieli
        if (kuvaosoite.length > 0)
            kirja.kuva = kuvaosoite
        kirjapalvelu
            .lisaa(kirja)
            .then(() => {
                tyhjennä()
                setPäivitä(true)
            })
        setOnnistui(true)
        setViesti(nimi + ' lisätty onnistuneesti')
        setTimeout(() => {
            setOnnistui(false)
        }, 3000)
    }

    return (
        <div className='pohja topattu-vähän topattu-vähän-lisää-alhaalta'>
            <Form onSubmit={lisaa}>
                <Form.Row className='justify-content-md-center'>
                    <Form.Group
                        as={Col}
                        md='4'
                        hasValidation
                    >
                        <Form.Control
                            id='nimiKenttä'
                            type='text'
                            value={nimi}
                            onChange={muuta}
                            placeholder='Kirjan nimi'
                            autocomplete='off'
                            isInvalid={virheet.nimi}
                        />
                    </Form.Group>
                    <Form.Group
                        as={Col}
                        md='4'
                        hasValidation
                    >
                        <Form.Control
                            id='kirjailijaKenttä'
                            type='text'
                            value={kirjailija}
                            onChange={muuta}
                            placeholder='Kirjailijan nimi'
                            autocomplete='off'
                            isInvalid={virheet.kirjailija}
                        />
                    </Form.Group>
                </Form.Row>
                <Form.Row className='justify-content-md-center'>
                    <Form.Group
                        as={Col}
                        md='4'
                        hasValidation
                    >
                        <Form.Control
                            id='kieliKenttä'
                            type='text'
                            value={kieli}
                            onChange={muuta}
                            placeholder='Kirjan kieli'
                            autocomplete='off'
                            isInvalid={virheet.kieli}
                        />
                    </Form.Group>
                    <Form.Group as={Col} md='4'>
                        <Form.Control
                            id='kuvaosoiteKenttä'
                            type='text'
                            value={kuvaosoite}
                            onChange={muuta}
                            placeholder='Kansikuvan osoite'
                            autocomplete='off'
                        />
                    </Form.Group>
                </Form.Row>
                <Button className='nappi' size='lg' type='submit'>Lisää</Button>
                <Modal
                    centered
                    show={onnistui}
                    onHide={() => setOnnistui(false)}
                    size='sm'
                >
                    <Modal.Body>
                        {viesti}
                    </Modal.Body>
                </Modal>
            </Form>
        </div>
    )
}

export default LisaaKirja
