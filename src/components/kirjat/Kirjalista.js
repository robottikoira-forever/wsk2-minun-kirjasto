import React, {useEffect, useState} from 'react'
import kirjapalvelu from '../../services/kirjapalvelu'
import Kirja from './Kirja'
import {ListGroup} from 'react-bootstrap'
import Lainausikkuna from "../Lainausikkuna";

const Kirjalista = ({päivitä, setPäivitä, rajaus}) => {
    const [kirjat, setKirjat] = useState([])

    const näytetäänköKirja = (kirja) => {
        if (kirja.nimi.toLowerCase().indexOf(rajaus.haku.toLowerCase()) !== -1 || kirja.kirjailija.toLowerCase().indexOf(rajaus.haku.toLowerCase()) !== -1) {
            switch (rajaus.näytä) {
                case 'kaikki':
                    return true
                case 'vapaat':
                    return kirja.vapaa
                case 'lainatut':
                    return !kirja.vapaa
            }
        }
    }

    const lista = kirjat.filter(kirja => näytetäänköKirja(kirja)).map(kirja => {
        return (
            <Kirja
                key={kirja.id}
                kirja={kirja}
                setPäivitä={setPäivitä}
            />
        )
    })

    useEffect(() => {
        if (päivitä)
            kirjapalvelu
                .haeKaikki()
                .then(haetutKirjat => {
                    setKirjat(haetutKirjat)
                    setPäivitä(false)
                })
    }, [päivitä])

    return (
        <ListGroup>
            {lista}
        </ListGroup>
    )
}

export default Kirjalista