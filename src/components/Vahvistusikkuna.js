import React from "react";
import {Modal, Button} from "react-bootstrap";

const Vahvistusikkuna = ({otsikko, sisältö, näytä, sulje, vahvista}) => {

    return (
        <Modal
            show={näytä}
            centered
            size="lg"
            backdrop='static'>
            <Modal.Header>
                <Modal.Title>
                    {otsikko}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {sisältö}
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={sulje} variant='light'>Peruuta</Button>
                <Button onClick={() => {vahvista();sulje()}} className='nappi-petrooli'>Vahvista</Button>
            </Modal.Footer>
        </Modal>
    )
}

export default Vahvistusikkuna