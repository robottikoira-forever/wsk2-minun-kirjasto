import React, {useState, useEffect} from 'react'
import {Button} from "react-bootstrap";
import Vahvistusikkuna from "../Vahvistusikkuna";
import lainapalvelu from "../../services/lainapalvelu";

const Laina = ({laina, setPäivitä}) => {
    const [avaaPalautus, setAvaaPalautus] = useState(false)
    const [palauta, setPalauta] = useState(false)

    const päivämäärä = () => {
        const pvm = new Date()
        return `${pvm.getFullYear()}-${pvm.getMonth() + 1}-${pvm.getDate()}`
    }
    const muunnaNätiksiPäivämääräksi = (datapvm) => {
        const pvm = new Date(datapvm)
        return `${pvm.getDate()}.${pvm.getMonth() + 1}.${pvm.getFullYear()}`
    }

    const {id, kirja_id, lainaaja_id, kirja_nimi, lainaaja_nimi} = laina
    const lainauspäivä = laina.lainauspaiva
    const palautuspäivä = laina.palautuspaiva

    const palautuselementti = palautuspäivä !== null
        ? muunnaNätiksiPäivämääräksi(palautuspäivä)
        : <Button className='nappi' onClick={() => setAvaaPalautus(true)}>Palauta</Button>

    const sisältö = `Onko ${lainaaja_nimi} palauttanut kirjan ${kirja_nimi}?`

    useEffect(() => {
        if (palauta) {
            const palautuspäivä = päivämäärä()
            lainapalvelu
                .palauta(palautuspäivä, id).then(() => {
                setPäivitä(true);
                setPalauta(false)
            })
        }
    }, [palauta])

    return (
        <>
            <tr>
                <td>{kirja_nimi}</td>
                <td>{lainaaja_nimi}</td>
                <td>{muunnaNätiksiPäivämääräksi(lainauspäivä)}</td>
                <td>{palautuselementti}</td>
            </tr>
            <Vahvistusikkuna
                näytä={avaaPalautus}
                sulje={() => setAvaaPalautus(false)}
                otsikko='Vahvista kirjan palautus'
                sisältö={sisältö}
                vahvista={() => setPalauta(true)}
            />
        </>
    )
}

export default Laina