import React, {useEffect, useState} from 'react'
import {Table} from 'react-bootstrap'
import lainapalvelu from '../../services/lainapalvelu'
import Laina from './Laina'

const Lainataulukko = () => {
    const [lainat, setLainat] = useState([])
    const [päivitä, setPäivitä] = useState(true)

    const taulu = lainat.map(laina => {
        return (
            <Laina
                key={laina.id}
                laina={laina}
                setPäivitä={setPäivitä}
            />
        )
    })

    useEffect(() => {
        if (päivitä)
            lainapalvelu
                .haeKaikki()
                .then(lainat => {
                    setLainat(lainat)
                    setPäivitä(false)
                })
    }, [päivitä])

    return (
        <Table className='pohja vaalea-teksti'>
            <thead className='pohja-vaalea'>
            <tr>
                <th>Kirja</th>
                <th>Lainaaja</th>
                <th>Lainauspäivä</th>
                <th>Palautuspäivä</th>
            </tr>
            </thead>
            <tbody>
            {taulu}
            </tbody>
        </Table>
    )
}

export default Lainataulukko