const mysql = require('mysql');
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const fs = require('fs')
const axios = require('axios')

const app = express();

const con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "testikirjasto"
});

app.use(cors({
    origin: 'http://localhost:3000'
}));

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

app.get('/kirjat', (pyyntö, vastaus) => {
    const sql = 'select id, nimi, kirjailija, kieli, kuva, ' +
        'not exists (select * from lainat where kirja_id = k.id and palautuspaiva is null) as vapaa from kirjat as k ' +
        'order by kirjailija, nimi, kieli'
    con.query(sql, [], (error, data) => vastaus.send(data))
});

app.get('/lainaajat', (pyyntö, vastaus) => {
    const sql = 'select * from lainaajat order by nimi'
    con.query(sql, [], (error, data) => vastaus.send(data))
});

app.get('/lainat', (pyyntö, vastaus) => {
    const sql = 'select k.nimi as kirja_nimi, lj.nimi as lainaaja_nimi, ln.id as id, lainauspaiva, palautuspaiva, kirja_id, lainaaja_id ' +
        'from lainat as ln ' +
        'left join lainaajat as lj on lainaaja_id = lj.id ' +
        'left join kirjat as k on kirja_id = k.id ' +
        'order by palautuspaiva is null desc, ' +
        'palautuspaiva desc, ' +
        'lainauspaiva desc'
    con.query(sql, [], (err, data) => vastaus.send(data))
});

app.post('/kirjat', (pyyntö, vastaus) => {
    const {nimi, kirjailija, kieli, kuva} = pyyntö.body
    const valinnaisetTaulukko = []
    let valinnaisetNimet = ''
    let valinnaisetArvot = ''

    if (kieli !== null) {
        valinnaisetTaulukko.push(kieli)
        valinnaisetNimet += ', kieli'
        valinnaisetArvot += ', ?'
    }
    if (kuva !== null) {
        const sijainnit = tallennussijainti(nimi)
        lataaKuva(kuva, sijainnit.lataus)
        valinnaisetTaulukko.push(sijainnit.tietokanta)
        valinnaisetNimet += ', kuva'
        valinnaisetArvot += ', ?'
    }
    const sql = `insert into kirjat (nimi, kirjailija${valinnaisetNimet}) values (?, ?${valinnaisetArvot})`
    con.query(sql, [nimi, kirjailija, ...valinnaisetTaulukko], (error, data) => vastaus.send(data))
    console.log('sql-lause:', sql)
    console.log('valinnaiset-taulukko:', valinnaisetTaulukko)
});

app.post('/lainaajat', (pyyntö, vastaus) => {
    const nimi = pyyntö.body.nimi
    const sql = 'insert into lainaajat (nimi) values (?)'
    con.query(sql, [nimi], (error, data) => vastaus.send(data))
});

app.post('/lainat', (pyyntö, vastaus) => {
    const {lainaaja_id, kirja_id, lainauspäivä} = pyyntö.body
    const sql = 'insert into lainat (lainaaja_id, kirja_id, lainauspaiva) values (?, ?, ?)'
    con.query(sql, [lainaaja_id, kirja_id, lainauspäivä], (error, data) => {
        vastaus.send(data)
        console.log(error)
    })
});

app.put('/lainat/:id', (pyyntö, vastaus) => {
    const sql = 'update lainat set palautuspaiva = ? where id = ?'
    const palautuspäivä = pyyntö.body.palautuspäivä
    const id = pyyntö.params.id
    console.log('body: ', pyyntö.body)
    console.log('params: ', pyyntö.params)
    con.query(sql, [palautuspäivä, id], (error, data) => vastaus.send(data))
});

app.delete('/kirjat/:id', (pyyntö, vastaus) => {
    const sql = 'delete from kirjat where id = ?'
    const id = pyyntö.params.id
    liittyvienLainojenPoisto('kirja_id', id)
    con.query(sql, [id], (error, data) => vastaus.send(data))
});

app.delete('/lainaajat/:id', (pyyntö, vastaus) => {
    const sql = 'delete from lainaajat where id = ?'
    const id = pyyntö.params.id
    liittyvienLainojenPoisto('lainaaja_id', id)
    con.query(sql, [id], (error, data) => vastaus.send(data))
});

const liittyvienLainojenPoisto = (kenttä, id) => {
    const sql = `delete from lainat where ${kenttä} = ?`
    con.query(sql, [id], (error, data) => console.log('Poisto onnistui'))
}

const tallennussijainti = (nimi) => {
    const kuvanNimi = `${nimi.split(' ').join('-')}`
    return {
        lataus: `./public/resources/${kuvanNimi}`,
        tietokanta: `/resources/${kuvanNimi}`
    }
}

const lataaKuva = async (kuvaosoite, tallennussijainti) => {
    const vastaus = await axios({
        url: kuvaosoite,
        method: 'GET',
        responseType: 'stream'
    })
    vastaus.data.pipe(fs.createWriteStream(tallennussijainti))
}

const server = app.listen(8081, () => {
    const port = server.address().port
    console.log('Server listening at http://localhost:' + port)
});